module github.com/sjqzhang/gmock

go 1.16

require (
	gitee.com/chunanyong/zorm v1.5.5
	github.com/alicebob/miniredis/v2 v2.20.0
	github.com/dolthub/go-mysql-server v0.11.0
	github.com/fsouza/go-dockerclient v1.7.10
	github.com/go-redis/redis/v8 v8.11.5
	github.com/go-sql-driver/mysql v1.6.0
	github.com/gomodule/redigo v1.8.8
	github.com/jinzhu/gorm v1.9.16
	github.com/mattn/go-sqlite3 v1.14.12
	gorm.io/driver/mysql v1.3.3
	gorm.io/driver/sqlite v1.3.2
	gorm.io/gorm v1.23.5
	xorm.io/xorm v1.3.0
)
